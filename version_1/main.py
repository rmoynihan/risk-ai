from game_agent import GameAgent
from game import Game

# Create game agents
agents = [ GameAgent('Woodford', (13,59,102)), GameAgent('Basil', (244,211,94)), GameAgent('Rose', (249,87,56)) ]

# Create game and start
game = Game(agents)
game.play()
