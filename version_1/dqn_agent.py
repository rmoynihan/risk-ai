import random
import numpy as np
import pandas as pd
from operator import add
from keras.optimizers import Adam
from keras.models import Sequential
from keras.layers.core import Dense, Dropout
from collections import deque

class DQNAgent:
    def __init__(self, game_agent):
        self.game_agent = game_agent
        self.reward = 0
        self.gamma = 0.9
        self.dataframe = pd.DataFrame()
        self.learning_rate = 0.0005
        self.model = self.create_network()
        self.epsilon = 0
        self.memory = deque(maxlen=50_000)

    def get_state(self):
        self.game_agent.game_state.update()
        return np.asarray(list(vars(self.game_agent.game_state).values())[2:])

    def set_reward(self, delta_territory):
        self.reward = 0
        if delta_territory == -1:
            self.reward -= 10
        elif delta_territory == 1:
            self.reward += 10

        return self.reward

    def create_network(self):
        model = Sequential()
        model.add(Dense(output_dim=120, activation='relu', input_dim=110))
        model.add(Dropout(0.15))
        model.add(Dense(output_dim=120, activation='relu'))
        model.add(Dropout(0.15))
        model.add(Dense(output_dim=120, activation='relu'))
        model.add(Dropout(0.15))
        model.add(Dense(output_dim=5, activation='softmax'))
        opt = Adam(self.learning_rate)
        model.compile(loss='mse', optimizer=opt)
        return model

    def remember(self, state, action, reward, next_state, done=False):
        self.memory.append((state, action, reward, next_state, done))

    def replay_new(self, memory):
        if (len(memory) > 1000):
            batch = random.sample(memory, 1000)
        else:
            batch = memory

        for state, action, reward, next_state, done in batch:
            target = reward
            if not done:
                target = reward + self.gamma * np.amax(self.model.predict(np.array([next_state]))[0])
            target_f = self.model.predict(np.array([state]))
            target_f[0][np.argmax(action)] = target
            self.model.fit(np.array([state]), target_f, epochs=1, verbose=0)

    def train_short_memory(self, state, action, reward, next_state, done=False):
        target = reward
        if not done:
            target = reward + self.gamma * np.amax(self.model.predict(next_state.reshape((1,110)))[0])
        target_f = self.model.predict(state.reshape((1,110)))
        target_f[0][np.argmax(action)] = target
        self.model.fit(state.reshape((1, 110)), target_f, epochs=1, verbose=0)
