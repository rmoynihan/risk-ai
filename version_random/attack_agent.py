import random

class AttackAgent:
    def __init__(self, game_agent):
        self.game_agent = game_agent

    def execute(self, game_state):
        actions = []

        # All possible targets
        for territory in game_state.get_controlled_territories():
            for border in territory.get_borders():
                if border not in game_state.get_controlled_territories():
                    # if territory.get_army_count() >= 2:
                    #     # Random for now...
                    #     do_attack = (random.random() < .75)
                    #     if do_attack:
                    #         actions = self.attack(territory, border, game_state, actions)
                    #         game_state.update()

                    do_attack = True
                    while do_attack:
                        if territory.get_army_count() >= 2:
                            # Random for now...
                            do_attack = (random.random() < .75)
                            if do_attack:
                                actions = self.attack(territory, border, game_state, actions)
                                game_state.update()

                            # Intelligence
                            # If num_dice > 0?...
                        else:
                            do_attack = False

        return actions

    # Perform and evaluate attack
    def attack(self, atk_territory, def_territory, game_state, actions):
        # Log
        actions.append(self.game_agent.get_name() + ' attacked ' + def_territory.get_name() + ' from ' + atk_territory.get_name())

        # Determine number of dice to roll
        num_atk_dice = random.randrange(0, min(atk_territory.get_army_count(), 3)) + 1
        num_def_dice = def_territory.get_owner().defend(atk_territory, def_territory, game_state)

        # Roll the dice
        atk_rolls = sorted([random.randrange(0, 6) + 1 for x in range(num_atk_dice)], reverse=True)
        def_rolls = sorted([random.randrange(0, 6) + 1 for x in range(num_def_dice)], reverse=True)

        actions.append(self.game_agent.get_name() + ' rolled ' + str(atk_rolls))
        actions.append(def_territory.get_owner().get_name() + ' rolled ' + str(def_rolls))

        # Determine results
        atk_losses = 0
        def_losses = 0
        conquer = None
        for x in range(min(num_atk_dice, num_def_dice)):
            if (atk_rolls[x] > def_rolls[x]):
                def_territory.remove_armies(1)
                def_losses += 1
            elif (def_rolls[x] > atk_rolls[x]):
                atk_territory.remove_armies(1)
                atk_losses += 1

            # Determine new control
            if def_territory.get_army_count() <= 0:
                def_territory.set_owner(self.game_agent)
                def_territory.set_army_count(1)
                atk_territory.remove_armies(1)
                conquer = (self.game_agent.get_name() + ' conquered ' + def_territory.get_name())
                break

        actions.append('Attacking losses: ' + str(atk_losses) + ' Defending losses: ' + str(def_losses))
        if conquer:
            actions.append(conquer)
        return actions

    # Defend against an attack
    def defend(self, atk_territory, def_territory, game_state):
        return random.randrange(0, min(atk_territory.get_army_count(), 3)) + 1
