from reinforce_agent import ReinforceAgent
from attack_agent import AttackAgent
from fortify_agent import FortifyAgent
#from defend_agent import DefendAgent

class GameAgent:
    def __init__(self, name, color):
        self.name = name
        self.color = color

        # Each game agent made of three independent decision agents
        self.reinforce_agent = ReinforceAgent(self)
        self.attack_agent = AttackAgent(self)
        self.fortify_agent = FortifyAgent(self)
        # self.defend_agent = DefendAgent(self)

        self.game_state = None

    def get_name(self):
        return self.name

    def get_color(self):
        return self.color

    def reinforce(self):
        actions = self.reinforce_agent.execute(self.game_state)
        self.phase_end(actions)

    def attack(self):
        actions = self.attack_agent.execute(self.game_state)
        self.phase_end(actions)

    def fortify(self):
        actions = self.fortify_agent.execute(self.game_state)
        self.phase_end(actions)

    def defend(self, atk_territory, def_territory, game_state):
        return self.attack_agent.defend(atk_territory, def_territory, game_state)

    def phase_end(self, actions):
        self.game_state.update()
        print(actions)
