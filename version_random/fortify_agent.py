import random

class FortifyAgent:
    def __init__(self, game_agent):
        self.game_agent = game_agent

    def execute(self, game_state):
        actions = []

        for territory in game_state.get_controlled_territories():
            for border in territory.get_borders():
                if border in game_state.get_controlled_territories() and territory.get_army_count() >= 2:
                    if random.random() < .2:
                        actions = self.fortify(territory, border, actions, game_state)
                        return actions

        return actions

    def fortify(self, territory, border, actions, game_state):
        transfer_amount = random.randrange(1, territory.get_army_count())
        territory.remove_armies(transfer_amount)
        border.add_armies(transfer_amount)

        actions.append(self.game_agent.get_name() + ' moved ' + str(transfer_amount) + ' armies from ' + territory.get_name() + ' to ' + border.get_name())
        return actions
