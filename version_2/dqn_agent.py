import random
import numpy as np
import pandas as pd
from operator import add
from keras.optimizers import Adam
from keras.models import Sequential
from keras.layers.core import Dense, Dropout
from collections import deque

class DQNAgent:
    def __init__(self, game_agent, output_dim, gamma=.9, learning_rate=.0005, memory_size=50000, batch_size=500):
        # Required params
        self.game_agent = game_agent
        self.output_dim = output_dim

        # Optional params
        self.gamma = gamma
        self.learning_rate = learning_rate
        self.memory = deque(maxlen=memory_size)
        self.batch_size = batch_size

        # Reward vars
        self.reward = 0
        self.gain_reward = 10
        self.loss_reward = -5
        self.stall_reward = -1

        # Neural net
        self.model = self.create_network()

        # Last action this agent took
        self.last_action = None

    # Return state as a numpy array
    def get_state(self):
        self.game_agent.game_state.update()
        return np.asarray(list(vars(self.game_agent.game_state).values())[2:])

    # Set the agent's reward
    def set_reward(self, delta_territory):
        self.reward = 0
        if delta_territory == -1:
            self.reward = self.gain_reward
        elif delta_territory == 1:
            self.reward = self.loss_reward
        else:
            self.reward = self.stall_reward

        return self.reward

    # Create the network
    def create_network(self):
        model = Sequential()
        model.add(Dense(output_dim=120, activation='relu', input_dim=167))
        model.add(Dropout(0.15))
        model.add(Dense(output_dim=120, activation='relu'))
        model.add(Dropout(0.15))
        model.add(Dense(output_dim=120, activation='relu'))
        model.add(Dropout(0.15))
        model.add(Dense(output_dim=self.output_dim, activation='softmax'))
        opt = Adam(self.learning_rate)
        model.compile(loss='mse', optimizer=opt)
        return model

    # Store state-action-state transition to memory
    def remember(self, state, action, reward, next_state, done=False):
        self.memory.append((state, action, reward, next_state, done))

    # Learn at end of game
    def replay_memories(self):
        # Get batch of memories
        if (len(self.memory) > self.batch_size):
            batch = random.sample(self.memory, self.batch_size)
        else:
            batch = self.memory

        # Re-fit model from memory batch
        for state, action, reward, next_state, done in batch:
            target = reward
            if not done:
                target = reward + self.gamma * np.amax(self.model.predict(np.array([next_state]))[0])
            target_f = self.model.predict(np.array([state]))
            target_f[0][np.argmax(action)] = target
            self.model.fit(np.array([state]), target_f, epochs=1, verbose=0)

    # Short term memory training
    def train_short_memory(self, state, action, reward, next_state, done=False):
        target = reward
        if not done:
            target = reward + self.gamma * np.amax(self.model.predict(next_state.reshape((1,167)))[0])
        target_f = self.model.predict(state.reshape((1,167)))
        target_f[0][np.argmax(action)] = target
        self.model.fit(state.reshape((1, 167)), target_f, epochs=1, verbose=0)
