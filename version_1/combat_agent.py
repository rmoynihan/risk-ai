import random
import numpy as np
from keras.utils import to_categorical

from dqn_agent import DQNAgent

class CombatAgent:
    def __init__(self, game_agent):
        self.game_agent = game_agent
        self.decide_attack_dqn = DQNAgent(game_agent)
        self.atk_dice_dqn = DQNAgent(game_agent)
        self.def_dice_dqn = DQNAgent(game_agent)
        self.state_old = None
        self.state_new = None

    def execute(self, game_state):
        actions = []
        co_actions = (None, None, None)

        # All possible targets
        for territory in game_state.get_controlled_territories():
            for border in territory.get_borders():
                if border not in game_state.get_controlled_territories():

                    # Attack loop (keep attacking until it decides not to)
                    do_attack = True
                    while do_attack:
                        # Check if we have enough units to attack
                        if territory.get_army_count() >= 2:
                            # DQN Method
                            self.state_old = self.decide_attack_dqn.get_state()

                            # Random or not?
                            if random.randrange(0, 500) < self.game_agent.epsilon:
                                # Random
                                do_attack = (random.random() < .5)
                                if do_attack:
                                    actions = self.attack(territory, border, game_state, actions)
                            else:
                                # DQN
                                prediction = self.decide_attack_dqn.model.predict(self.state_old.reshape((1, 110)))
                                action = to_categorical(np.argmax(prediction[0]), num_classes=2)

                                if np.array_equal(action, [1, 0]):
                                    actions = self.attack(territory, border, game_state, actions)
                                else:
                                    do_attack = False

                        # Not enough units left to attack
                        else:
                            do_attack = False

        return actions

    # Perform and evaluate attack
    def attack(self, atk_territory, def_territory, game_state, actions):
        # Log
        actions.append(self.game_agent.get_name() + ' attacked ' + def_territory.get_name() + ' from ' + atk_territory.get_name())

        # Determine number of dice to roll

        # Random or not?
        num_atk_dice = 1
        while (num_atk_dice <= atk_territory.get_army_count() - 1):
            self.state_old = self.atk_dice_dqn.get_state()
            if random.randrange(0, 500) < self.game_agent.epsilon:
                # Random
                num_atk_dice = random.randrange(0, min(atk_territory.get_army_count(), 3)) + 1
                break
            else:
                # DQN
                prediction = self.atk_dice_dqn.model.predict(self.state_old.reshape((1, 110)))
                action = to_categorical(np.argmax(prediction[0]), num_classes=2)

                if np.array_equal(action, [1, 0]):
                    # Add another die
                    num_atk_dice += 1
                else:
                    # Done adding die
                    break

        # Defending territory
        num_def_dice = def_territory.get_owner().defend(atk_territory, def_territory, game_state)

        # Roll the dice
        atk_rolls = sorted([random.randrange(0, 6) + 1 for x in range(num_atk_dice)], reverse=True)
        def_rolls = sorted([random.randrange(0, 6) + 1 for x in range(num_def_dice)], reverse=True)

        actions.append(self.game_agent.get_name() + ' rolled ' + str(atk_rolls))
        actions.append(def_territory.get_owner().get_name() + ' rolled ' + str(def_rolls))

        # Determine results
        atk_losses = 0
        def_losses = 0
        conquer = None
        for x in range(min(num_atk_dice, num_def_dice)):
            if (atk_rolls[x] > def_rolls[x]):
                def_territory.remove_armies(1)
                def_losses += 1
            elif (def_rolls[x] > atk_rolls[x]):
                atk_territory.remove_armies(1)
                atk_losses += 1

            # Determine new control
            if def_territory.get_army_count() <= 0:
                def_territory.set_owner(self.game_agent)
                def_territory.set_army_count(1)
                atk_territory.remove_armies(1)
                conquer = (self.game_agent.get_name() + ' conquered ' + def_territory.get_name())
                break

        actions.append('Attacking losses: ' + str(atk_losses) + ' Defending losses: ' + str(def_losses))
        if conquer:
            actions.append(conquer)
        return actions

    # Defend against an attack
    def defend(self, atk_territory, def_territory, game_state):
        # Determine number of dice to roll

        # Random or not?
        num_def_dice = 1
        while (num_def_dice <= def_territory.get_army_count()):
            self.state_old = self.def_dice_dqn.get_state()
            if random.randrange(0, 500) < self.game_agent.epsilon:
                # Random
                num_def_dice = random.randrange(0, min(def_territory.get_army_count(), 2)) + 1
                break
            else:
                # DQN
                prediction = self.def_dice_dqn.model.predict(self.state_old.reshape((1, 110)))
                action = to_categorical(np.argmax(prediction[0]), num_classes=2)

                if np.array_equal(action, [1, 0]):
                    # Add another die
                    num_def_dice += 1
                else:
                    # Done adding die
                    break

        return num_def_dice
