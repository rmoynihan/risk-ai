from game_agent import GameAgent
from game import Game

# Create game agents
agents = [ GameAgent('Woodford', (13,59,102)), GameAgent('Basil', (244,211,94), always_random=False) ]#, GameAgent('Rose', (249,87,56), always_random=True) ]

# Create game and start
game = Game(agents)

user_exit = False
game_num = 0
while not user_exit:
    print("\n", '*' * 25)
    print("GAME #" + str(game_num))
    print()

    game.setup()
    num_attacks = 0

    user_exit = game.play()

    # Re-fit agents
    for agent in agents:
        agent.end_of_game()

    # Play again
    game_num += 1
