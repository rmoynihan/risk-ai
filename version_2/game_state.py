import numpy as np
from pprint import pprint

class GameState:
    def __init__(self, game_map, owner):
        self.game_map = game_map
        self.owner = owner

        #############
        # META GAME #
        #############

        self.turn = 0
        self.num_players = 0

        ##############
        # AGGREGATES #
        ##############

        self.num_territories_owned = 0
        self.total_armies_owned = 0

        ###############
        # TERRITORIES #
        ###############

        # self.control territory codes:
        # [0]: Controlled by this agent
        # [1]: Controlled by agent with fewer total armies
        # [2]: Controlled by agent with more total armies

        # Initialize territory state variables to defaults
        for territory in self.game_map.territories:
            setattr(self, 'control_' + territory.get_name().replace(' ', '_'), 0)
            setattr(self, territory.get_name() + '_army_count', territory.get_army_count())
            setattr(self, territory.get_name() + '_attacked_last_turn', territory.was_attacked())

        ##############
        # CONTINENTS #
        ##############

        # self.own continent codes:
        # [0]: Not owned by this agent
        # [1]: Owned by this agent
        for cont in self.game_map.continents:
            setattr(self, 'control_' + cont, 0)

    def update(self):
        # Reset aggregate values
        self.num_players = 0
        self.num_territories_owned = 0
        self.total_armies_owned = 0

        players = {}
        conts = {cont: 0 for cont in self.game_map.continents}

        # For each territory, determine state and increase aggregates
        for territory in self.game_map.territories:
            this_name = territory.get_name().replace(' ', '_')
            this_owner = territory.get_owner()
            this_army_count = territory.get_army_count()
            this_was_attacked = territory.was_attacked()

            # Add player and army count to dictionary
            if (this_owner in players.keys()):
                players[this_owner] += this_army_count
            else:
                players[this_owner] = this_army_count

            # Determine state info
            if (this_owner == self.owner):
                # Aggregates
                self.num_territories_owned += 1
                self.total_armies_owned += this_army_count
                conts[territory.get_cont()] += 1

                # Owned by this agent
                setattr(self, 'control_' + this_name, 0)
            else:
                # If not owned by this agent, is owner a higher or lower threat?
                if (this_owner.game_state.total_armies_owned >= self.total_armies_owned):
                    setattr(self, 'control_' + this_name, 2)
                else:
                    setattr(self, 'control_' + this_name, 1)

            # Get the army count for this territory
            setattr(self, this_name + '_army_count', this_army_count)

            # Check if territory was attacked last turn
            setattr(self, this_name + '_attacked_last_turn', this_was_attacked)

        # Total number of players remaining
        self.num_players = len(players)

        # Continent control
        if conts['na'] == 9: self.control_na = 1
        if conts['sa'] == 4: self.control_sa = 1
        if conts['eu'] == 7: self.control_eu = 1
        if conts['as'] == 12: self.control_as = 1
        if conts['au'] == 5: self.control_au = 1
        if conts['af'] == 6: self.control_af = 1

    # Pretty print game state
    def print_state(self):
        pprint(vars(self))
        print('Number of game state attributes: ' + len(vars(self))[2:])

    # Returns all territories controlled by this agent
    def get_controlled_territories(self):
        territories = []
        for territory in self.game_map.get_territories():
            if (self.owner == territory.get_owner()):
                territories.append(territory)
        return territories

    # Get state as numpy array
    def get_state(self):
        self.update()
        return np.asarray(list(vars(self).values())[2:])
