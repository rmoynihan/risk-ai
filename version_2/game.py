import pygame
import sys
import random
import numpy as np

from game_map import GameMap
from territory import Territory
from game_state import GameState

class Game:
    # Initialize the game environment
    def __init__(self, agents):
        pygame.init()

        # VARS
        self.display_width = 1280
        self.display_height = 720
        self.black = (0, 0, 0)
        self.bkgr_color = (230, 232, 230)
        self.agents = agents
        self.stale_limit = 500

        # Pygame meta
        self.game_display = pygame.display.set_mode((self.display_width, self.display_height))
        pygame.display.set_caption("Skynet Training")

    def setup(self):
        # Draw game map
        self.game_display.fill(self.bkgr_color)
        self.game_map = GameMap(self.game_display)

        # Game setup
        for territory in self.game_map.territories:
            territory.set_owner(random.choice(self.agents))
            territory.set_army_count(1)

        # Initialize game states for each agent
        for agent in self.agents:
            agent.game_state = GameState(self.game_map, agent)

        # First update for game states
        for agent in self.agents:
            agent.game_state.update()

    # Start the game loop
    def play(self):
        game_over = False

        # Game loop
        stale_count = 0
        while not game_over:
            # Game logic
            stale, game_over = self.step()

            # Check if the round was stale
            if stale: stale_count += 1

            # Check exit
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    game_over = True
                    return True

            # Too many consecutive stale rounds, start new game
            if stale_count > self.stale_limit:
                print("Stalled Out")
                game_over = True

        return False

    # Each round of the game
    def step(self):
        for agent in self.agents:
            agent.round_start()

        for agent in self.agents:
            agent.start_turn()
            agent.reinforce()
            agent.attack()
            agent.fortify()
            agent.end_turn()

        for agent in self.agents:
            agent.round_end()

        # Refresh the map
        self.game_map.refresh_map()

        # Check game over conditions
        # Either agents have done nothing, or an agent has one the game
        stale = False
        num_intel_agents = 0
        for agent in self.agents:
            # Round was stale
            stale = (stale or agent.stale_round)

            # Check if agent is using intelligence
            if not agent.always_random and agent.game_state.num_territories_owned > 0: num_intel_agents += 1

            # An agent has won the game:
            if agent.game_state.num_players == 1 and agent.game_state.num_territories_owned > 0:
                print(agent.name + " Won")
                return False, True

        # If all intelligent players are gone, call game over
        if num_intel_agents == 0:
            print("All intelligent agents eliminated.")
            return False, True

        # Keep the game going
        return stale, False

    def exit(self):
        # Exit game
        pygame.display.quit()
        pygame.quit()
        sys.exit()
