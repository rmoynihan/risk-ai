import pygame
import sys
import random

from game_map import GameMap
from territory import Territory
from game_state import GameState

class Game:
    # Initialize the game environment
    def __init__(self, agents):
        pygame.init()

        # VARS
        self.display_width = 1280
        self.display_height = 720
        self.black = (0, 0, 0)
        self.white = (255, 255, 255)
        self.agents = agents

        # Pygame meta
        self.game_display = pygame.display.set_mode((self.display_width, self.display_height))
        pygame.display.set_caption("Skynet Training")

        # Draw game map
        self.game_display.fill(self.white)
        self.game_map = GameMap(self.game_display)

        # Game setup
        for territory in self.game_map.get_territories():
            territory.set_owner(random.choice(self.agents))
            territory.set_army_count(1)

        # Initialize game states for each agent
        for agent in self.agents:
            agent.game_state = GameState(self.game_map, agent)

        # First update for game states
        for agent in self.agents:
            agent.game_state.update()

    # Start the game loop
    def play(self):
        game_over = False

        # Game loop
        while not game_over:
            # Game logic
            game_over = self.step()

            # Check exit
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    game_over = True

    # Each round of the game
    def step(self):
        for agent in self.agents:
            agent.start_turn()
            agent.reinforce()
            agent.attack()
            agent.fortify()
            agent.turn_end()

        # Refresh the map
        self.game_map.refresh_map()

        # Check game over conditions
        return False

    def exit(self):
        # Exit game
        pygame.display.quit()
        pygame.quit()
        sys.exit()
