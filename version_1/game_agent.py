from reinforce_agent import ReinforceAgent
from combat_agent import CombatAgent
from fortify_agent import FortifyAgent
#from defend_agent import DefendAgent

class GameAgent:
    def __init__(self, name, color):
        self.name = name
        self.color = color

        # Each game agent made of three independent decision agents
        self.reinforce_agent = ReinforceAgent(self)
        self.combat_agent = CombatAgent(self)
        self.fortify_agent = FortifyAgent(self)
        # self.defend_agent = DefendAgent(self)

        self.game_state = None
        self.epsilon = 500

        self.prev_territory = 0
        self.state_old = None
        self.state_new = None

        self.re_action = None
        self.co_actions = None

    def get_name(self):
        return self.name

    def get_color(self):
        return self.color

    def start_turn(self):
        self.prev_territory = len(self.game_state.get_controlled_territories())
        self.state_old = self.reinforce_agent.dqn.get_state()

    def reinforce(self):
        actions, self.re_action = self.reinforce_agent.execute(self.game_state)
        self.epsilon = 500 - self.game_state.turn
        self.phase_end(actions)

    def attack(self):
        actions, self.co_actions = self.combat_agent.execute(self.game_state)
        # self.combat_agent.dqn.epsilon = 500 - self.game_state.turn
        self.phase_end(actions)

    def fortify(self):
        actions = self.fortify_agent.execute(self.game_state)
        # self.fortify_agent.dqn.epsilon = 500 - self.game_state.turn
        self.phase_end(actions)

    def defend(self, atk_territory, def_territory, game_state):
        return self.combat_agent.defend(atk_territory, def_territory, game_state)

    def phase_end(self, actions):
        self.game_state.update()
        print(actions)

    def turn_end(self):
        # Get new state
        self.state_new = self.reinforce_agent.dqn.get_state()

        # Determine gain or loss in territory
        delta_territory = 0
        if len(self.game_state.get_controlled_territories()) > self.prev_territory:
            delta_territory = 1
        elif len(self.game_state.get_controlled_territories()) < self.prev_territory:
            delta_territory = -1

        # Reinforce Agent
        reward = self.reinforce_agent.dqn.set_reward(delta_territory)
        self.reinforce_agent.dqn.train_short_memory(self.state_old,  self.re_action, reward, self.state_new)
        self.reinforce_agent.dqn.remember(self.state_old, self.re_action, reward, self.state_new)

        # Attack Agent(s)
        reward = self.combat_agent.decide_attack_dqn.set_reward(delta_territory)
        self.combat_agent.decide_attack_dqn.train_short_memory(self.state_old,  self.re_action, reward, self.state_new)
        self.combat_agent.decide_attack_dqn.remember(self.state_old, self.re_action, reward, self.state_new)

        self.game_state.turn += 1
