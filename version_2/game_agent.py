import numpy as np
from random import random, randint
from scipy import stats
from keras.utils import to_categorical

from game_state import GameState
from dqn_agent import DQNAgent
import game_functions as gf

class GameAgent:
    def __init__(self, name, color, always_random=False):
        # Agent UI
        self.name = name
        self.color = color
        self.always_random = always_random

        # Epsilon settings
        self.epsilon_max = 2500
        self.epsilon = self.epsilon_max

        # This agent's GameState object
        self.game_state = None

        # Agent made of four separate DQNs
        self.num_re_actions = 43
        self.reinforce_agent = DQNAgent(self, self.num_re_actions)

        self.num_atk_actions = 164
        self.attack_agent = DQNAgent(self, self.num_atk_actions)

        self.num_def_actions = 2
        self.defend_agent = DQNAgent(self, self.num_def_actions)

        self.num_fort_actions = 164
        self.fortify_agent = DQNAgent(self, self.num_fort_actions)

        self.all_agents = [self.reinforce_agent, self.attack_agent, self.defend_agent, self.fortify_agent]

        # Numpy arrays that hold GameState snapshots
        self.state_old = None
        self.state_new = None

        # Agent locked up
        self.stale_round = False

    def round_start(self):
        self.state_old = self.game_state.get_state()

    def start_turn(self):
        pass

    def reinforce(self):
        # Log
        phase_log = []

        # Determine random action or predicted action
        if randint(0, self.epsilon_max) < self.epsilon or self.always_random:
            # Random
            prediction = np.array( [[(random()/self.num_re_actions) for x in range(self.num_re_actions)]] )
            # action = to_categorical(randint(0,1), num_classes=self.num_re_actions)
        else:
            # Predicted
            prediction = self.reinforce_agent.model.predict(self.state_old.reshape((1, 167)))
            # action = to_categorical(np.argmax(prediction[0]), num_classes=self.num_re_actions)

        # Set the agent's action
        self.reinforce_agent.last_action = prediction

        ####################################
        # Interpret the prediction and act #
        ####################################
        # Treat exact predicted Q value as precentage of commitment

        # Convert predictions to z-score values
        z_scores = stats.zscore(prediction[0])

        # Get number of available reinforcements
        reinforce_count = gf.calc_reinforce_count(self.game_state)

        # For each index in the array represents a territory, can be matched to it's ID
        for t_code in range(z_scores.size):
            # Get the territory
            territory = self.game_state.game_map.get_territory_by_id(t_code)

            # Calculate commitment size
            num_armies = round(z_scores.item(t_code))

            # If we own it, commit the armies and log action
            if territory.get_owner() == self and num_armies > 0:
                territory.add_armies(num_armies)
                phase_log.append(self.name + ' added ' + str(num_armies) + ' to ' + territory.get_name())
                reinforce_count -= num_armies
            else:
                pass
                # phase_log.append(self.name + ' wanted to add ' + str(num_armies) + ' to ' + territory.get_name() + " but CAN'T")

        # Print the phase log
        self.print_log(phase_log)

    def attack(self):
        # Log
        phase_log = []

        # Determine random action or predicted action
        if randint(0, self.epsilon_max) < self.epsilon or self.always_random:
            # Random
            prediction = np.array( [[(random()/self.num_atk_actions) for x in range(self.num_atk_actions)]] )
            # action = to_categorical(randint(0,1), num_classes=self.num_atk_actions)
        else:
            # Predicted
            prediction = self.attack_agent.model.predict(self.state_old.reshape((1, 167)))
            # action = to_categorical(np.argmax(prediction[0]), num_classes=self.num_atk_actions)

        # Set the agent's action
        self.attack_agent.last_action = prediction

        ####################################
        # Interpret the prediction and act #
        ####################################
        # Convert probabilities to z-scores and use those to determine attack actions and strengths

        # Convert predictions to z-score values
        z_scores = stats.zscore(prediction[0])

        # Iterate through every possible attack scenario
        i = 0
        for atk_territory in self.game_state.game_map.territories:
            for def_territory in atk_territory.get_borders():
                if z_scores.item(i) > 2:
                    # full attack
                    phase_log = gf.attack(phase_log, self.game_state, atk_territory, def_territory, full=True)
                elif z_scores.item(i) > 0:
                    # half attack
                    phase_log = gf.attack(phase_log, self.game_state, atk_territory, def_territory, full=False)
                else:
                    # no attack
                    pass

        # Print the phase log
        self.print_log(phase_log)

    def fortify(self):
        phase_log = []

    def defend(self, def_territory, game_state):
        return randint(1,2)

    def phase_end(self):
        pass

    # Actions to be performed at the end of every turn (just this agent's actions in the round)
    def end_turn(self):
        self.stale_round = np.array_equal(self.state_old, self.game_state.get_state())

    # Actions to be performed at the end of every round (meaning all agents have gone)
    def round_end(self):
        self.state_new = self.game_state.get_state()

        # Return 1 if overall gain of territory, -1 if loss of territory
        delta_territory = (np.sign([self.state_new.item(2) - self.state_old.item(2)])).item(0)

        if not self.always_random:
            for agent in self.all_agents:
                reward = agent.set_reward(delta_territory)
                agent.train_short_memory(self.state_old,  agent.last_action, reward, self.state_new)
                agent.remember(self.state_old, agent.last_action, reward, self.state_new)

        self.game_state.turn += 1

    # Actions to be performed at the conclusion of each game
    def end_of_game(self):
        if not self.always_random:
            for agent in self.all_agents:
                agent.replay_memories()

        # Reset epsilon
        self.epsilon -= 1

    def print_log(self, log):
        for entry in log:
            print(entry)
