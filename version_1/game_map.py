import pygame

from territory import Territory

class GameMap:
    def __init__(self, game_display):
        self.game_display = game_display
        self.territories, self.continents = self.create_territories()
        self.refresh_map()

    def draw_lines(self):
        black = (0,0,0)

        # North America
        pygame.draw.line(self.game_display, black, (25,100), (50,75))     # Alaska - NA
        pygame.draw.line(self.game_display, black, (275,75), (450,50))    # NWT - Greenland
        pygame.draw.line(self.game_display, black, (300,100), (450,50))   # Ontario - Greenland
        pygame.draw.line(self.game_display, black, (400,100), (450,50))   # Quebec - Greenland

        # South America
        pygame.draw.line(self.game_display, black, (425,400), (500,325))  # Brazil - North Africa

        # Europe
        pygame.draw.line(self.game_display, black, (550,50), (600,100))   # Greenland - Iceland
        pygame.draw.line(self.game_display, black, (625,125), (625,138.5))# Iceland - Britain
        pygame.draw.line(self.game_display, black, (650,100), (675,138.5))# Iceland - Scandanavia
        pygame.draw.line(self.game_display, black, (625,188.5), (675,200))# Britain - Western EU
        pygame.draw.line(self.game_display, black, (725,150), (725,175))  # Scandanavia - Northern EU
        pygame.draw.line(self.game_display, black, (650,250), (675,275))  # Western EU - Egypt
        pygame.draw.line(self.game_display, black, (650,250), (575,275))  # Western EU - North Africa

        # Russia/Asia
        pygame.draw.line(self.game_display, black, (1100,250), (1150,275))# Mongolia - Japan
        pygame.draw.line(self.game_display, black, (1175,200), (1175,225))# Kamchatka - Japan
        pygame.draw.line(self.game_display, black, (1105,225), (1150,250))# Irkutsk - Japan
        pygame.draw.line(self.game_display, black, (1225,150), (1250,100))# Kamchatka - NA
        pygame.draw.line(self.game_display, black, (950,425), (975,450))  # Siam - Indonesia

        # Australia
        pygame.draw.line(self.game_display, black, (1025,475), (1075,500))# Indonesia - New Guinea
        pygame.draw.line(self.game_display, black, (975,500), (1000,550)) # Indonesia - Western AU
        pygame.draw.line(self.game_display, black, (1100,525), (1075,550))# New Guinea - Eastern AU
        pygame.draw.line(self.game_display, black, (1150,600), (1175,625))# Eastern AU - New Zealand

        # Africa
        pygame.draw.line(self.game_display, black, (650,575), (675,575))  # South Africa - Madagascar
        pygame.draw.line(self.game_display, black, (700,525), (700,550))  # East Africa - Madagascar

    def create_territories(self):
        ###########################
        # INSTANTIATE TERRITORIES #
        ###########################

        # North America
        alaska = Territory(self.game_display, 'Alaska', 'na', 50, 50, 75, 75)
        north_west_territory = Territory(self.game_display, 'North West Territory', 'na', 125, 50, 150, 50)
        alberta = Territory(self.game_display, 'Alberta', 'na', 125, 100, 100, 75)
        ontario = Territory(self.game_display, 'Ontario', 'na', 225, 100, 100, 75)
        quebec = Territory(self.game_display, 'Quebec', 'na', 325, 100, 100, 75)
        greenland = Territory(self.game_display, 'Greenland', 'na', 450, 0, 100, 75)
        western_us = Territory(self.game_display, 'Western US', 'na', 150, 175, 125, 75)
        eastern_us = Territory(self.game_display, 'Eastern US', 'na', 275, 175, 125, 75)
        central_america = Territory(self.game_display, 'Central America', 'na', 175, 250, 100, 100)

        # South America
        venezuela = Territory(self.game_display, 'Venezuela', 'sa', 250, 350, 100, 50)
        peru = Territory(self.game_display, 'Peru', 'sa', 225, 400, 100, 100)
        brazil = Territory(self.game_display, 'Brazil', 'sa', 325, 400, 100, 150)
        argentina = Territory(self.game_display, 'Argentina', 'sa', 250, 500, 75, 150)

        # Europe
        iceland = Territory(self.game_display, 'Iceland', 'eu', 600, 75, 50, 50)
        britain = Territory(self.game_display, 'Britian', 'eu', 600, 138.5, 50, 50)
        western_europe = Territory(self.game_display, 'Western EU', 'eu', 625, 200, 75, 50)
        southern_europe = Territory(self.game_display, 'Southern EU', 'eu', 700, 225, 75, 50)
        northern_europe = Territory(self.game_display, 'Northern EU', 'eu', 700, 175, 75, 50)
        scandanavia = Territory(self.game_display, 'Scandanavia', 'eu', 675, 100, 100, 50)
        ukraine = Territory(self.game_display, 'Ukraine', 'eu', 775, 125, 75, 150)

        # Russia/China
        middle_east = Territory(self.game_display, 'Middle East', 'as', 725, 275, 125, 50)
        afghanistan = Territory(self.game_display, 'Afghanistan', 'as', 850, 225, 75, 50)
        india = Territory(self.game_display, 'India', 'as', 850, 275, 75, 125)
        ural = Territory(self.game_display, 'Ural', 'as', 850, 100, 75, 125)
        siberia = Territory(self.game_display, 'Siberia', 'as', 925, 75, 100, 100)
        yakutsk = Territory(self.game_display, 'Yakutsk', 'as', 1025, 100, 100, 75)
        kamchatka = Territory(self.game_display, 'Kamchatka', 'as', 1125, 125, 100, 75)
        irkutsk = Territory(self.game_display, 'Irkutsk', 'as', 925, 175, 200, 50)
        mongolia = Territory(self.game_display, 'Mongolia', 'as', 925, 225, 175, 50)
        china = Territory(self.game_display, 'China', 'as', 925, 275, 150, 75)
        siam = Territory(self.game_display, 'Siam', 'as', 925, 350, 50, 75)
        japan = Territory(self.game_display, 'Japan', 'as', 1150, 225, 50, 75)

        # Australia
        indonesia = Territory(self.game_display, 'Indonesia', 'au', 925, 450, 100, 50)
        new_guinea = Territory(self.game_display, 'New Guinea', 'au', 1075, 475, 75, 50)
        western_australia = Territory(self.game_display, 'Western au', 'au', 950, 550, 100, 100)
        eastern_australia = Territory(self.game_display, 'Eastern au', 'au', 1050, 550, 100, 100)
        new_zealand = Territory(self.game_display, 'NZ', 'au', 1175, 600, 50, 75)

        # Africa
        north_africa = Territory(self.game_display, 'North Africa', 'af', 500, 275, 150, 150)
        egypt = Territory(self.game_display, 'Egypt', 'af', 650, 275, 75, 75)
        east_africa = Territory(self.game_display, 'East Africa', 'af', 650, 350, 100, 175)
        congo = Territory(self.game_display, 'Congo', 'af', 550, 425, 100, 100)
        south_africa = Territory(self.game_display, 'South Africa', 'af', 550, 525, 100, 125)
        madagascar = Territory(self.game_display, 'MG', 'af', 675, 550, 50, 75)

        ###############
        # SET BORDERS #
        ###############

        # North America
        alaska.set_borders( [kamchatka, north_west_territory, alberta] )
        north_west_territory.set_borders( [alaska, alberta, ontario, greenland] )
        alberta.set_borders( [alaska, north_west_territory, ontario, western_us] )
        ontario.set_borders( [north_west_territory, alberta, greenland, quebec, western_us, eastern_us] )
        quebec.set_borders( [ontario, greenland, eastern_us] )
        greenland.set_borders( [north_west_territory, ontario, quebec, iceland] )
        western_us.set_borders( [alberta, ontario, central_america, eastern_us] )
        eastern_us.set_borders( [western_us, ontario, quebec, central_america] )
        central_america.set_borders( [western_us, eastern_us, venezuela] )

        # South America
        venezuela.set_borders( [central_america, peru, brazil] )
        peru.set_borders( [venezuela, brazil, argentina] )
        brazil.set_borders( [venezuela, peru, argentina] )
        argentina.set_borders( [peru, brazil] )

        # Europe
        iceland.set_borders( [greenland, britain, scandanavia] )
        britain.set_borders( [iceland, western_europe] )
        western_europe.set_borders( [britain, north_africa, egypt, northern_europe, southern_europe] )
        southern_europe.set_borders( [western_europe, northern_europe, egypt, middle_east, ukraine] )
        northern_europe.set_borders( [western_europe, southern_europe, ukraine, scandanavia] )
        scandanavia.set_borders( [iceland, northern_europe, ukraine] )
        ukraine.set_borders( [scandanavia, northern_europe, southern_europe, middle_east, ural, afghanistan, india] )

        # Russia/China
        middle_east.set_borders( [egypt, southern_europe, ukraine, india, afghanistan] )
        afghanistan.set_borders( [ural, irkutsk, mongolia, china, india, middle_east, ukraine] )
        india.set_borders( [afghanistan, mongolia, china, siam, middle_east, ukraine] )
        ural.set_borders( [ukraine, siberia, irkutsk, mongolia, afghanistan] )
        siberia.set_borders( [ural, yakutsk, irkutsk] )
        yakutsk.set_borders( [siberia, kamchatka, irkutsk] )
        kamchatka.set_borders( [alaska, japan, yakutsk, irkutsk] )
        irkutsk.set_borders( [siberia, yakutsk, kamchatka, japan, mongolia, afghanistan, ural] )
        mongolia.set_borders( [irkutsk, japan, china, india, afghanistan, ural] )
        china.set_borders( [mongolia, siam, india, afghanistan] )
        siam.set_borders( [china, india, indonesia] )
        japan.set_borders( [kamchatka, irkutsk, mongolia] )

        # Australia
        indonesia.set_borders( [siam, new_guinea, western_australia] )
        new_guinea.set_borders( [indonesia, eastern_australia] )
        western_australia.set_borders( [indonesia, eastern_australia] )
        eastern_australia.set_borders( [new_guinea, western_australia, new_zealand] )
        new_zealand.set_borders( [eastern_australia] )

        # Africa
        north_africa.set_borders( [western_europe, egypt, east_africa, congo, brazil] )
        egypt.set_borders( [western_europe, southern_europe, middle_east, east_africa, north_africa] )
        east_africa.set_borders( [egypt, madagascar, congo, north_africa] )
        congo.set_borders( [north_africa, east_africa, south_africa] )
        south_africa.set_borders( [congo, east_africa, madagascar] )
        madagascar.set_borders( [east_africa, south_africa] )

        #########################
        # APPEND TO TERRITORIES #
        #########################
        territories = [
        alaska, north_west_territory, alberta, ontario, quebec, greenland, western_us, eastern_us, central_america,
        venezuela, peru, brazil, argentina,
        iceland, britain, scandanavia, western_europe, northern_europe, southern_europe, ukraine,
        middle_east, ural, afghanistan, india, siberia, yakutsk, kamchatka, irkutsk, mongolia, japan, china, siam,
        indonesia, new_guinea, western_australia, eastern_australia, new_zealand,
        north_africa, egypt, east_africa, congo, south_africa, madagascar
        ]

        return territories, ['na', 'sa', 'eu', 'as', 'au', 'af']

    def get_territories(self):
        return self.territories

    def get_continents(self):
        return self.continents

    def change_owner(self, territory, new_owner):
        territory.set_owner(new_owner)

    def set_army_count(self, territory, army_count):
        territory.set_army_count(army_count)

    def refresh_map(self):
        # Erase screen contents
        self.game_display.fill((255,255,255))

        self.draw_lines()

        for territory in self.territories:
            territory.draw()

        pygame.display.update()
