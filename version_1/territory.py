import pygame

class Territory:
    def __init__(self, game_display, name, cont, x, y, width, height):
        self.game_display = game_display
        self.name = name
        self.cont = cont
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.borders = []

        self.owner = None
        self.color = (0,0,0)
        self.army_count = 0

        pygame.font.init()
        self.font = pygame.font.Font('freesansbold.ttf', 11)

        self.draw()

    def draw(self):
        # Rectangle
        pygame.draw.rect(self.game_display, self.color, (self.x, self.y, self.width, self.height), 2)

        # Name
        text = self.font.render(self.name, False, (0,0,0))
        self.game_display.blit(text, (self.x + 5, self.y + 5))

        # Army count
        text = self.font.render(str(self.army_count), False, (0,0,0))
        self.game_display.blit(text, (self.x + (self.width/2), self.y + (self.height/2)))

    def get_name(self):
        return self.name

    def get_cont(self):
        return self.cont

    def get_borders(self):
        return self.borders

    def set_borders(self, borders):
        for b in borders:
            self.borders.append(b)

    def get_owner(self):
        return self.owner

    def set_owner(self, new_owner):
        self.owner = new_owner
        self.color = new_owner.get_color()

    def get_army_count(self):
        return self.army_count

    def set_army_count(self, count):
        if count < 100:
            self.army_count = count
        else:
            self.army_count = 100

    def add_armies(self, count):
        if self.army_count < 100:
            self.army_count += count

    def remove_armies(self, count):
        self.army_count -= count
