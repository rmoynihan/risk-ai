from game_agent import GameAgent
from game import Game

# Create game agents
agents = [ GameAgent('Woodford', (255,0,0)), GameAgent('Basil', (0,255,0)), GameAgent('Rose', (0,0,255)) ]
# agents = [ GameAgent('Supreme Overlord', (255,0,255)) ]

# Create game and start
game = Game(agents)
game.play()
