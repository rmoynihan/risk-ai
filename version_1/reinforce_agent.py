import random
import numpy as np
from keras.utils import to_categorical

from dqn_agent import DQNAgent

class ReinforceAgent:
    def __init__(self, game_agent):
        self.game_agent = game_agent
        self.dqn = DQNAgent(self.game_agent)
        self.state_old = None
        self.state_new = None

    def execute(self, game_state):
        actions = []
        action = None
        reinforce_count = self.calc_reinforce_count(game_state)

        # For all territories that this agent owns...
        while reinforce_count > 0 and len(game_state.get_controlled_territories()) > 0:
            for territory in game_state.get_controlled_territories():
                if reinforce_count == 0:
                    break

                # Reinforcement decisions
                else:
                    # DQN Method
                    self.state_old = self.dqn.get_state()

                    # Random or not?
                    if random.randrange(0, 500) < self.game_agent.epsilon:
                        # Random
                        action = to_categorical(random.randint(0, 2), num_classes=5)
                    else:
                        # DQN
                        prediction = self.dqn.model.predict(self.state_old.reshape((1, 110)))
                        action = to_categorical(np.argmax(prediction[0]), num_classes=5)

                        if (np.array_equal(action, [1,0,0,0,0])):
                            r = 0.00
                        elif (np.array_equal(action, [0,1,0,0,0])):
                            r = 0.25
                        elif (np.array_equal(action, [0,0,1,0,0])):
                            r = 0.50
                        elif (np.array_equal(action, [0,0,0,1,0])):
                            r = 0.75
                        elif (np.array_equal(action, [0,0,0,0,1])):
                            r = 1.00

                        # Add reinforcements
                        territory.add_armies(round(reinforce_count * r))
                        if r > 0:
                            actions.append(self.game_agent.get_name() + ' added ')

        return actions, action

    def calc_reinforce_count(self, game_state):
        if game_state.turn == 1:
            return 40 - (game_state.num_players * 5)

        # Get initial count from territory holdings
        count = game_state.num_territories_owned // 3

        # Continent bonuses
        if game_state.control_as: count += 7
        if game_state.control_na: count += 5
        if game_state.control_eu: count += 5
        if game_state.control_af: count += 3
        if game_state.control_sa: count += 2
        if game_state.control_au: count += 2

        if count < 3:
            count = 3

        return count
