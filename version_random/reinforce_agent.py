import random

class ReinforceAgent:
    def __init__(self, game_agent):
        self.game_agent = game_agent

    def execute(self, game_state):
        actions = []
        reinforce_count = self.calc_reinforce_count(game_state)

        # For all territories that this agent owns...
        while reinforce_count > 0 and len(game_state.get_controlled_territories()) > 0:
            for territory in game_state.get_controlled_territories():
                if reinforce_count == 0:
                    break

                # Randomly choose
                else:
                    if random.random() < .2:
                        r = random.randrange(1, reinforce_count+1)
                        territory.add_armies(r)
                        reinforce_count -= r
                        actions.append(self.game_agent.get_name() + ' added ' + str(r) + ' to ' + territory.get_name())
                        game_state.update()

                # Intelligence
                # return number for reinforcement count (including 0)

        return actions

    def calc_reinforce_count(self, game_state):
        if game_state.turn == 1:
            return 40 - (game_state.num_players * 5)

        # Get initial count from territory holdings
        count = game_state.num_territories_owned // 3

        # Continent bonuses
        if game_state.control_as: count += 7
        if game_state.control_na: count += 5
        if game_state.control_eu: count += 5
        if game_state.control_af: count += 3
        if game_state.control_sa: count += 2
        if game_state.control_au: count += 2

        return count
