from random import random, randint

def calc_reinforce_count(game_state):
    if game_state.turn == 1:
        return 40 - (game_state.num_players * 5)

    # Get initial count from territory holdings
    count = game_state.num_territories_owned // 3

    # Continent bonuses
    if game_state.control_as: count += 7
    if game_state.control_na: count += 5
    if game_state.control_eu: count += 5
    if game_state.control_af: count += 3
    if game_state.control_sa: count += 2
    if game_state.control_au: count += 2

    # Everyone gets at least 3 reinforcements
    if count < 3:
        count = 3

    return count

def attack(phase_log, game_state, atk_territory, def_territory, full):
    # Make sure this is a valid attack
    if atk_territory.get_army_count() <= 2 or atk_territory.get_owner() != game_state.owner or def_territory.get_owner() == game_state.owner:
        return phase_log

    phase_log.append(game_state.owner.name + ' moves to attack ' + def_territory.get_name())

    # Get number of attacking dice and defending dice
    num_atk_dice = min(3, atk_territory.get_army_count()-1)
    num_def_dice = def_territory.get_owner().defend(def_territory, game_state)

    # Roll the dice
    atk_rolls = sorted([randint(1, 6) for x in range(num_atk_dice)], reverse=True)
    def_rolls = sorted([randint(1, 6) for x in range(num_def_dice)], reverse=True)

    # Log
    phase_log.append(game_state.owner.name + ' rolled ' + str(atk_rolls))
    phase_log.append(def_territory.get_owner().name + ' rolled ' + str(def_rolls))

    # Determine results
    atk_losses = 0
    def_losses = 0
    conquer = None
    for x in range(min(num_atk_dice, num_def_dice)):
        if (atk_rolls[x] > def_rolls[x]):
            def_territory.remove_armies(1)
            def_losses += 1
        elif (def_rolls[x] > atk_rolls[x]):
            atk_territory.remove_armies(1)
            atk_losses += 1

        # Determine new control
        if def_territory.get_army_count() <= 0:
            def_territory.set_owner(game_state.owner)
            def_territory.set_army_count(1)
            atk_territory.remove_armies(1)
            conquer = (game_state.owner.name + ' conquered ' + def_territory.get_name())
            break

    # Log
    phase_log.append('Attacking losses: ' + str(atk_losses) + ' Defending losses: ' + str(def_losses))
    if conquer:
        phase_log.append(conquer)

    return phase_log
